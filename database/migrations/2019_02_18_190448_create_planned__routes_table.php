<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlannedRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planned__routes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('location_id');
            $table->integer('planned_stop_id');
            $table->string('max_stop');
            $table->string('min_stop');
            $table->bigInteger('stop_duration');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planned__routes');
    }
}
