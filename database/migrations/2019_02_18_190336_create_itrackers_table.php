<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItrackersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itrackers', function (Blueprint $table) {
            $table->increments('id');
            $table->date('itDate');
            $table->time('itTime');
            $table->float('itTemp');
            $table->float('itHumidity');
            $table->float('itLuminosity');
            $table->mediumInteger('itAcceleration_x');
            $table->integer('itAcceleration_Y');
            $table->integer('itAcceleration_Z');
            $table->string('itA2C_List');
            $table->char('itSeal_Status');
            $table->tinyInteger('itGeo_Position');
            $table->string('itLatitude');
            $table->char('itLatitude_Type');
            $table->string('itLongitutde');
            $table->char('itLongitutde_Type');
            $table->string('itSpeed');
            $table->string('itPrecision');
            $table->string('trID');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itrackers');
    }
}
