<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->string('location_type');
            $table->string('location_category');
            $table->string('dropoff_address');
            $table->string('name');
            $table->string('description');
            $table->string('opening_hours');
            $table->string('website');
            $table->string('longitude');
            $table->char('longitude_type');
            $table->string('latitude');
            $table->char('latitude_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
