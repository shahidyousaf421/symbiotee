<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceAlertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device__alerts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('alert_collection_id');
            $table->integer('order_id');
            $table->integer('device_id');
            $table->integer('alert_type_id');
            $table->string('sensor_name');
            $table->string('sensor_value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device__alerts');
    }
}
