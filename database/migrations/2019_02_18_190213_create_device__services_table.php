<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device__services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('device_type_id');
            $table->integer('company_id');
            $table->string('status');
            $table->date('lease_start');
            $table->date('lease_end');
            $table->string('frequency');
            $table->string('sampling');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device__services');
    }
}
