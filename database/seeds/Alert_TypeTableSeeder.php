<?php

use Illuminate\Database\Seeder;

class Alert_TypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name'=>'Monitering','description'=>'Monitering','device_sensor_id'=>1],
            ['name'=>'Monitering','description'=>'Monitering','device_sensor_id'=>1],
            ['name'=>'Monitering','description'=>'Monitering','device_sensor_id'=>1],
            ['name'=>'Monitering','description'=>'Monitering','device_sensor_id'=>1],
            ['name'=>'Monitering','description'=>'Monitering','device_sensor_id'=>1],
            ['name'=>'Monitering','description'=>'Monitering','device_sensor_id'=>1],
        ];

        foreach ($data as $key => $value){
            \App\Alert_Type::create($value);
        }
    }
}
