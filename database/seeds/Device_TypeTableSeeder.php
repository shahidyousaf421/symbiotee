<?php

use Illuminate\Database\Seeder;

class Device_TypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name'=>'JoinTech','description'=>'IoT Device'],
            ['name'=>'iTracker','description'=>'IoT Device'],
        ];

        foreach ($data as $key => $value)
        {
            \App\Device_Type::create($value);
        }
    }
}
