<?php

use Illuminate\Database\Seeder;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name'=>'Symbiotee','description'=>'Service Provider','address'=>'Grenoble, France','email'=>'remy.noirot@symbiotee.com','phone'=>'+33660085562','status'=>'active'],
            ['name'=>'Amazon','description'=>'Customer','address'=>'Lyon, France','email'=>'remy.noirot@symbiotee.com','phone'=>'+331566046','status'=>'active'],
            ['name'=>'SCLI Roanne','description'=>'Transporter','address'=>'Roanne, france','email'=>'remy.noirot@symbiotee.com','phone'=>'+33477783535','status'=>'active'],
            ['name'=>'DHL','description'=>'Transporter Customer','address'=>'Grenoble, France','email'=>'remy.noirot@symbiotee.com','phone'=>'+33660085562','status'=>'active'],
        ];
        foreach ($data as $key => $value) {
            \App\Company::create($value);
        }
    }
}
