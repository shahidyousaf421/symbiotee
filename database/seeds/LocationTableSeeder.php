<?php

use Illuminate\Database\Seeder;

class LocationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
            	'company_id'=>4,
            	'location_type'=>'Pick and Drop',
            	'location_category'=>'Standard',
            	'dropoff_address'=>'14 RUE GRANDE, 36170 Saint-Benoît-du-Sault, France',
            	'name'=>'DHL Express Mag Presse ST BENOIT DE SAULT',
            	'description'=>'DHL France',
            	'opening_hours'=>'9.00AM till 5.00PM',
            	'website'=>'https://mydhl.express.dhl/',
            	'longitude'=>'46.4409',
            	'longitude_type'=>'N',
            	'latitude'=>'1.3896',
            	'latitude_type'=>'E'
    		],
    		[
            	'company_id'=>4,
            	'location_type'=>'Pick and Drop',
            	'location_category'=>'Standard',
            	'dropoff_address'=>'33 RUE ROUVIER C.CIAL SUPER ITM MERCOEUR, 63100 Clermont-Ferrand, France',
            	'name'=>'DHL Express Mag Presse CLERMONT FERRAND',
            	'description'=>'DHL France',
            	'opening_hours'=>'9.00AM till 5.00PM',
            	'website'=>'https://mydhl.express.dhl/',
            	'longitude'=>'45.7772',
            	'longitude_type'=>'N',
            	'latitude'=>'3.0870',
            	'latitude_type'=>'E'
    		],
    		[
            	'company_id'=>3,
            	'location_type'=>'Pick and Drop',
            	'location_category'=>'Standard',
            	'dropoff_address'=>'18 RUE IRENEE CARRE 08000 - CHARLEVILLE MEZIERES',
            	'name'=>'SCLI Charleville-Mézières',
            	'description'=>'Transportation Company',
            	'opening_hours'=>'9.00AM till 5.00PM',
            	'website'=>'https://www.scli42.com/',
            	'longitude'=>'49.4543506',
            	'longitude_type'=>'N',
            	'latitude'=>'44.333946',
            	'latitude_type'=>'E'
    		],
    		[
            	'company_id'=>3,
            	'location_type'=>'Pick and Drop',
            	'location_category'=>'Standard',
            	'dropoff_address'=>'16 Place Jean Jaurès, 33220 Sainte-Foy-la-Grande, France',
            	'name'=>'SCLI Relay Sainte-Foy-la-Grande',
            	'description'=>'Pickup and Dropoff',
            	'opening_hours'=>'9.00AM till 5.00PM',
            	'website'=>'https://www.scli42.com/',
            	'longitude'=>'44.8418',
            	'longitude_type'=>'N',
            	'latitude'=>'0.2150',
            	'latitude_type'=>'E'
    		],
    		
        ];

        foreach ($data as $key => $value)
        {
            \App\Location::create($value);
        }
    }
}
