<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(Alert_TypeTableSeeder::class);
        $this->call(CompanyTableSeeder::class);
        $this->call(Device_TypeTableSeeder::class);
        $this->call(LocationTableSeeder::class);
    }
}
