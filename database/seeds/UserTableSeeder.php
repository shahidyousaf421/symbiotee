<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name'=>'remy','email'=>'remy@mail.com','user_type'=>'Admin','avatar'=>'default-user.png
','password'=>bcrypt('123456')],
            ['name'=>'stephane','email'=>'stephane@mail.com','user_type'=>'Customer','avatar'=>'default-user.png
','password'=>bcrypt('123456')],
            ['name'=>'Sophie','email'=>'Sophie@mail.com','user_type'=>'Manager','avatar'=>'default-user.png
','password'=>bcrypt('123456')],
            ['name'=>'ayesha','email'=>'ayesha@mail.com','user_type'=>'Manager','avatar'=>'default-user.png
','password'=>bcrypt('123456')],
            ['name'=>'kashif','email'=>'kashif@mail.com','user_type'=>'Diver','avatar'=>'default-user.png
','password'=>bcrypt('123456')],
            ['name'=>'shahid','email'=>'shahid@mail.com','user_type'=>'Customer','avatar'=>'default-user.png
','password'=>bcrypt('123456')],
        ];
        foreach ($data as $key => $value) {
            \App\User::create($value);
        }
    }
}
