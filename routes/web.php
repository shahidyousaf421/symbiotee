<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clearcache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1> : '. phpversion();
});

// Route::post('/logout',function(){
// 	// require view('auth.login');
// 	dd('fsdfssdf');
// })->name('logout');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'HomeController@index');

//Profile Routes
Route::get('/profile','ProfileController@index')->name('profile-view');
Route::post('/profile/update/detail','ProfileController@profile_update_detail')->name('profile-update-detail');
Route::post('/profile/update/avatar','ProfileController@profile_update_avatar')->name('profile-update-avatar');
Route::post('/profile/update/password','ProfileController@profile_update_password')->name('profile-update-password');

//Customer Routes Start
Route::get('/order/create/view','OrderController@create_order_view')->name('create-order-view');
Route::post('/order/create','OrderController@create_order')->name('create-order');
Route::get('/order/all','OrderController@all_order')->name('all-order');
Route::get('/order/edit/view','OrderController@edit_order_view')->name('edit-order-view');
Route::post('/order/edit','OrderController@edit_order')->name('edit-order');
Route::get('/order/delete','OrderController@delete_order')->name('delete-order');

Route::get('/track/order/view','OrderController@track_order_view')->name('track-order-view');
Route::get('/track/order','OrderController@track_order')->name('track-order');


//Manager Routes
Route::get('/duty/create/view','DutyController@create_duty_view')->name('create-duty-view');
Route::post('/duty/create','DutyController@create_duty')->name('create-duty');
Route::get('/duty/all','DutyController@all_duty')->name('all-duty');
Route::get('/duty/edit/view','DutyController@edit_duty_view')->name('edit-duty-view');
Route::post('/duty/edit','DutyController@edit_duty')->name('edit-duty');
Route::get('/duty/delete','DutyController@delete_duty')->name('delete-duty');

Route::get('/track/duty/view','DutyController@track_duty_view')->name('track-duty-view');
Route::get('/track/duty','DutyController@track_duty')->name('track-duty');


//Admin Routes
Route::get('/user/create/view','UserController@create_user_view')->name('create-user-view');
Route::get('/user/edit/view','UserController@edit_user_view')->name('edit-user-view');
Route::post('/user/create','UserController@create_user')->name('create-user');
Route::post('/user/edit','UserController@edit_user')->name('edit-user');
Route::get('/user/delete','UserController@delete_user')->name('delete-user');
Route::get('/user/password/reset','UserController@pas_rest_user')->name('pas-rest-user');
Route::get('/user/all','UserController@all_user')->name('all-user');

Route::get('/vehicle/create/view','VehicleController@create_vehicle_view')->name('create-vehicle-view');
Route::post('/vehicle/create','VehicleController@create_vehicle')->name('create-vehicle');
Route::get('/vehicle/edit/view','VehicleController@edit_vehicle_view')->name('edit-vehicle-view');
Route::Post('/vehicle/edit','VehicleController@edit_vehicle')->name('edit-vehicle');
Route::get('/vehicle/delete','VehicleController@delete_vehicle')->name('delete-vehicle');
Route::get('/vehicle/all','VehicleController@all_vehicle')->name('all-vehicle');

Route::get('/Device/create/view','Device_ServiceController@create_device_view')->name('create-device-view');
Route::post('/Device/create','Device_ServiceController@create_device')->name('create-device');
Route::get('/Device/all','Device_ServiceController@all_device')->name('all-device');
Route::get('/Device/edit/view','Device_ServiceController@edit_device_view')->name('edit-device-view');
Route::get('/Device/delete','Device_ServiceController@delete_device')->name('delete-device');
Route::post('/Device/edit','Device_ServiceController@edit_device')->name('edit-device');

Route::get('/Device/Sensor/create/view','Device_SensorController@create_sensor_view')->name('create-sensor-view');
Route::post('/Device/Sensor/create','Device_SensorController@create_sensor')->name('create-sensor');
Route::get('/Device/Sensor/all','Device_SensorController@all_sensor')->name('all-sensor');
Route::get('/Device/Sensor/edit/view','Device_SensorController@edit_sensor_view')->name('edit-sensor-view');
Route::get('/Device/Sensor/delete','Device_SensorController@delete_sensor')->name('delete-sensor');
Route::post('/Device/Sensor/edit','Device_SensorController@edit_sensor')->name('edit-sensor');

Route::get('/Alert/create/view','AlertController@create_alert_view')->name('create-alert-view');
Route::post('/Alert/create','AlertController@create_alert')->name('create-alert');
Route::get('/Alert/all','AlertController@all_alert')->name('all-alert');
Route::get('/Alert/edit/view','AlertController@edit_alert_view')->name('edit-alert-view');
Route::get('/Alert/delete','AlertController@delete_alert')->name('delete-alert');
Route::post('/Alert/edit','AlertController@edit_alert')->name('edit-alert');


Route::get('/location/create/view','LocationController@create_location_view')->name('create-location-view');
