<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incident_Type extends Model
{
    protected $fillable = [
        'name'
    ];
}
