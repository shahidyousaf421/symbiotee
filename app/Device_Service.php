<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device_Service extends Model
{
    protected $fillable = [
        'name',
        'device_type_id',
        'company_id',
        'status',
        'lease_start',
        'lease_end',
        'frequency',
        'sampling'
    ];

    public function device_type()
    {
        return $this->belongsTo(Device_Type::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
