<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Duty extends Model
{
    protected $fillable = [
        'user_id',
        'vehicle_id',
        'driver_id',
        'planned_route_id',
        'departure',
        'journey_start_time',
        'status'
    ];

    public function vehicle()
    {
    	return $this->belongsTo(Vehicle::class);
    }

    public function driver()
    {
    	return $this->belongsTo(User::class,'driver_id','id');
    }

    public function user()
    {
    	return $this->belongsTo(User::class,'user_id','id');
    }

    public function planned_route()
    {
        return $this->belongsTo(Planned_route::class);
    }

    public function dropoff()
    {
    	return $this->hasMany(Planned_dropoff::class,'duty_id','id')->with('order');
    }
}
