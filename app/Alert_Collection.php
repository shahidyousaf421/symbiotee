<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alert_Collection extends Model
{
    protected $fillable = [
        'alert_type_id',
        'condition',
        'value'
    ];

    public function order()
    {
        return $this->hasOne(Order::class);
    }

    public function alert_type()
    {
    	return $this->belongsTo(Alert_Type::class);
    }
}
