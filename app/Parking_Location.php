<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parking_Location extends Model
{
    protected $fillable = [
        'name',
        'address',
        'status',
        'longitude',
        'longitude_type',
        'latitude',
        'latitude_type'
    ];
}
