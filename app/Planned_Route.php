<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Planned_Route extends Model
{
    protected $fillable = [
        'location_id',
        'planned_stop_id',
        'max_stop',
        'min_stop',
        'planned_dropoff_id',
        'stop_duration'
    ];

    public function duty()
    {
    	return $this->hasMany(Duty::class);
    }
}
