<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Planned_dropoff extends Model
{
    protected $fillable = [
        'duty_id',
        'order_id'
    ];

    public function duty()
    {
    	return $this->belongsTo(Duty::class);
    }

    public function order()
    {
    	return $this->belongsTo(Order::class)->with('drop_location');
    }

}
