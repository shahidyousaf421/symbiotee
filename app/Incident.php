<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incident extends Model
{
    protected $fillable = [
        'incident_type_id',
        'driver_id',
        'duty_id',
        'report',
        'description'
    ];
}
