<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'name',
        'description',
        'address',
        'email',
        'phone',
        'status'
    ];

    public function device_service()
    {
        return $this->hasMany(Device_Service::class);
    }

    public function order()
    {
        return $this->hasMany(Order::class);
    }
}
