<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Planned_Stop extends Model
{
    protected $fillable = [
        'name',
        'description',
        'longitude',
        'longitude_type',
        'latitude',
        'latitude_type'
    ];
}
