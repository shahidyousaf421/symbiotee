<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'order_type','company_id','pickup_location_id','dropoff_location_id','alert_collection_id'
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
    public function alert_collection()
    {
        return $this->belongsTo(Alert_Collection::class)->with('alert_type');
    }
    public function pick_location()
    {
        return $this->belongsTo(Location::class,'pickup_location','id');
    }
    public function drop_location()
    {
        return $this->belongsTo(Location::class,'dropoff_location','id');
    }

    public function alert_type()
    {
        return $this->belongsTo(Alert_Type::class,'alrt_type_id','id');
    }

    public function planned_dropoff()
    {
        return $this->hasMany(Planned_dropoff::class);
    }

}
