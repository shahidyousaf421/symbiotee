<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device_Sensor extends Model
{
    protected $fillable = [
        'sensor_id',
        'device_type_id',
        'name',
        'description',
        'note'
    ];

    public function device_type()
    {
        return $this->belongsTo(Device_Type::class);
    }

    public function alert_type()
    {
        return $this->hasMany(Alert_Type::class);
    }
}
