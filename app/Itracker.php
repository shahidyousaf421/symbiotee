<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Itracker extends Model
{
    protected $fillable = [
      'itDate',
      'itTime',
      'itTemp',
      'itHumidity',
      'itLuminosity',
      'itAcceleration_x',
      'itAcceleration_Y',
      'itAcceleration_Z',
      'itA2C_List',
      'itSeal_Status',
      'itGeo_Position',
      'itLatitude',
      'itLatitude_Type',
      'itLongitutde',
      'itLongitutde_Type',
      'itSpeed',
      'itPrecision',
      'trID'
    ];
}
