<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alert_Type extends Model
{
    protected $fillable = [
        'name',
        'description',
        'device_sensor_id'
    ];

    public function device_sensor()
    {
        return $this->belongsTo(Device_Sensor::class);
    }

    public function alert_collection()
    {
        return $this->hasMany(Alert_Collection::class);
    }
}
