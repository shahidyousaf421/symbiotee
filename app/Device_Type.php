<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device_Type extends Model
{
    protected $fillable = [
        'name',
        'description'
    ];

    public function device_service()
    {
        return $this->hasMany(Device_Service::class);
    }

    public function device_sensor()
    {
        return $this->hasMany(Device_Sensor::class);
    }
}
