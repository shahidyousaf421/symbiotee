<?php

namespace App\Http\Controllers;

use App\Vehicle;
use Illuminate\Http\Request;

class VehicleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('prevent-back-history');
    }

    public function create_vehicle_view()
    {
        return view('pages.admin.vehicle.create_vehicle');
    }

    public function create_vehicle(Request $request)
    {
        $vehicle = Vehicle::newModelInstance($request->all());
        $vehicle->save();

        $notification = array(
            'message' => 'Vehicle Created Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }

    public function all_vehicle()
    {
        $all_vehicle = Vehicle::all();
        return view('pages.admin.vehicle.all_vehicle',compact('all_vehicle'));
    }

    public function edit_vehicle_view(Request $request)
    {
        $vehicle = Vehicle::whereId($request->get('id'))->first();
        return view('pages.admin.vehicle.edit_vehicle',compact('vehicle'));
    }

    public function edit_vehicle(Request $request)
    {
        Vehicle::whereId($request->get('id'))->update([
            'name' => $request['name'],
            'model' => $request['model'],
            'number' => $request['number']
        ]);

        $all_vehicle = Vehicle::all();

        $notification = array(
            'message' => 'Vehicle Updated Successfully',
            'alert-type' => 'success'
        );
        return redirect('vehicle/all')->with($notification,$all_vehicle);
    }

    public function delete_vehicle(Request $request)
    {
        $vehicle = Vehicle::find($request->get('id'));
        $vehicle->delete();

        $notification = array(
            'message' => 'Vehicle Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }
}
