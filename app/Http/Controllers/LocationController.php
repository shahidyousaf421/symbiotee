<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LocationController extends Controller
{
    public function create_location_view()
    {
    	return view('pages.admin.location.create_location');
    }
}
