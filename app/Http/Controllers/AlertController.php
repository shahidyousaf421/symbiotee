<?php

namespace App\Http\Controllers;

use App\Alert_Type;
use App\Device_Sensor;
use App\Device_Type;
use Illuminate\Http\Request;

class AlertController extends Controller
{
    public function create_alert_view()
    {
        $device_sensors = Device_Sensor::all();
        return view('pages.admin.alert.create_alert',compact('device_sensors'));
    }

    public function create_alert(Request $request)
    {
        $alert = Alert_Type::newModelInstance($request->all());
        $alert->save();

        $notification = array(
            'message' => 'Alert Created Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }

    public function all_alert()
    {
        $all_alerts = Alert_Type::with('device_sensor')->get();
        return view('pages.admin.alert.all_alert', compact('all_alerts'));
    }

    public function edit_alert_view(Request $request)
    {
        $alert = Alert_Type::whereId($request->get('id'))->with('device_sensor')->first();
        $device_sensors = Device_Sensor::all();
        return view('pages.admin.alert.edit_alert', compact('alert','device_sensors'));
    }

    public function delete_alert(Request $request)
    {
        $alert = Alert_Type::find($request->get('id'));
        $alert->delete();

        $notification = array(
            'message' => 'Alert Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }

    public function edit_alert(Request $request)
    {
        Alert_Type::whereId($request->get('id'))->update([
            'name' => $request['name'],
            'description' => $request['description'],
            'device_sensor_id' => $request['device_sensor_id']
        ]);

        $all_alerts = Alert_Type::with('device_sensor')->get();

        $notification = array(
            'message' => 'Alert Updated Successfully',
            'alert-type' => 'success'
        );

        return redirect('/Alert/all')->with($notification,$all_alerts);
    }
}
