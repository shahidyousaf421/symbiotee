<?php

namespace App\Http\Controllers;


use App\Company;
use App\Device_Service;
use App\Device_Type;
use Illuminate\Http\Request;

class Device_ServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('prevent-back-history');
    }
    public function create_device_view()
    {
        $device_types = Device_Type::all();
        $companies = Company::all();
        return view('pages.admin.device_service.create_device',compact('device_types','companies'));
    }

    public function create_device(Request $request)
    {
        $device = Device_Service::newModelInstance($request->all());
        $device->save();

        $notification = array(
            'message' => 'Device Service Created Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }

    public function all_device()
    {
        $all_devices = Device_Service::with('company')->with('device_type')->get();
        return view('pages.admin.device_service.all_devices', compact('all_devices'));
    }

    public function edit_device_view(Request $request)
    {
        $device = Device_Service::whereId($request->get('id'))->with('company')->with('device_type')->first();
        $device_types = Device_Type::all();
        $companies = Company::all();
        return view('pages.admin.device_service.edit_device', compact('device','device_types','companies'));
    }

    public function delete_device(Request $request)
    {
        $device = Device_Service::find($request->get('id'));
        $device->delete();

        $notification = array(
            'message' => 'Device Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }

    public function edit_device(Request $request)
    {
        Device_Service::whereId($request->get('id'))->update([
            'name' => $request['name'],
            'device_type_id' => $request['device_type_id'],
            'company_id' => $request['company_id'],
            'status' => $request['status'],
            'lease_start' => $request['lease_start'],
            'lease_end' => $request['lease_end'],
            'frequency' => $request['frequency'],
            'sampling' => $request['sampling']
        ]);

        $all_devices = Device_Service::with('company')->with('device_type')->get();

        $notification = array(
            'message' => 'Device Service Updated Successfully',
            'alert-type' => 'success'
        );

        return redirect('Device/all')->with($notification,$all_devices);
    }
}
