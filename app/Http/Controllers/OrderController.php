<?php

namespace App\Http\Controllers;

use App\Alert_Type;
use App\Company;
use App\Device_Service;
use App\location;
use App\Alert_Collection;
use App\Order;
use GMaps;
use Mapper;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('prevent-back-history');
    }
    public function create_order_view()
    {
        $devices = Device_Service::all();
        $companies = Company::all();
        $alerts = Alert_Type::all();
        $locations = Location::all();
        return view('pages.customer.create_order',compact('devices','companies','alerts','locations'));
    }

    public function create_order(Request $request)
    {
        // dd($request);
        $inputs = $request->all();
        $alert = Alert_Collection::newModelInstance();
        $alert->alert_type_id = $inputs['alert_type'];
        $alert->condition = $inputs['alert_condition'];
        $alert->value = $inputs['alert_value'];
        $alert->save();

        $alert_id = Alert_Collection::select('id')->max('id');

        $order = Order::newModelInstance();
        $order->order_type = $inputs['order_type'];
        $order->company_id = $inputs['company_id'];
        $order->pickup_location = $inputs['pickup_location_id'];
        $order->dropoff_location = $inputs['dropoff_location_id'];
        $order->alert_collection_id = $alert_id;
        $order->save();

        $notification = array(
            'message' => 'Order Created Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }

    public function all_order()
    {
        $all_orders = Order::with('company')->with('alert_collection')->with('pick_location')->with('drop_location')->get();
        // dd($all_orders);
        return view('pages.customer.all_order',compact('all_orders'));
    }

    public function edit_order_view(Request $request)
    {
        $order = Order::whereId($request->get('id'))->with('company')->with('alert_collection')->with('pick_location')->with('drop_location')->first();
        $devices = Device_Service::all();
        $companies = Company::all();
        $alerts = Alert_Type::all();
        $locations = Location::all();
        // dd($order);
        return view('pages.customer.edit_order',compact('order','devices','companies','alerts','locations'));

    }

    public function edit_order(Request $request)
    {
        $inputs = $request->all();
        Alert_Collection::whereId($request->get('a_id'))->update([
            'alert_type_id' => $inputs['alert_type'],
            'condition' => $inputs['alert_condition'],
            'value' => $inputs['alert_value']
        ]);

        Order::whereId($request->get('id'))->update([
            'order_type' => $inputs['order_type'],
            'company_id' => $inputs['company_id'],
            'pickup_location' => $inputs['pickup_location_id'],
            'dropoff_location' => $inputs['dropoff_location_id']
        ]);

        $notification = array(
            'message' => 'Order Updated Successfully',
            'alert-type' => 'success'
        );

        $all_orders = Order::with('company')->with('alert_collection')->with('pick_location')->with('drop_location')->get();
        return view('pages.customer.all_order',compact('all_orders','notification'));
    }

    public function delete_order(Request $request)
    {
        // dd($request);
        $alert = Alert_Collection::find($request->get('a_id'));
        $alert->delete();

        $order = Order::find($request->get('id'));
        $order->delete();

        $notification = array(
            'message' => 'Order Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }

    public function track_order_view()
    {
        // $config = array();
        // $config['center'] = 'auto';
        // $config['onboundschanged'] = 'if (!centreGot) {
        //     var mapCentre = map.getCenter();
        //     marker_0.setOptions({
        //         position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng())
        //     });
        // }
        // centreGot = true;';

        // GMaps::initialize($config);

        // $marker['position'] = 'center';
        // GMaps::add_marker($marker);
        
        // $map = GMaps::create_map();

        $orders = Order::all();
        $single_order = array('order_type' =>'p'); 

        // dd($orders);
        // $config['center'] = '48.855510,2.365505';
        $config['center'] = 'auto';
        GMaps::initialize($config);
        $map = GMaps::create_map();
        return view('pages.customer.track_order',compact('map','orders','single_order'));
    }

    public function track_order(Request $request)
    {
        $orders = Order::all(); 
        $single_order = Order::whereId($request->get('id'))->with('pick_location')->with('drop_location')->first();

        $config['center'] = 'France';
        $config['directions'] = true;
        $config['directionsMode'] = "DRIVING";

        $config['directionsStart'] = $single_order->pick_location->dropoff_address;
        $config['directionsEnd'] = $single_order->drop_location->dropoff_address;
        GMaps::initialize($config);
        $map = GMaps::create_map();
        return view('pages.customer.track_order',compact('map','orders','single_order'));
    }
}
