<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Planned_Route;
use App\Planned_dropoff;
use App\Duty;

class ApiController extends Controller
{
	function apiResponse($status, $type, $data, $optional = array())
    {
        if ($status) {
            if ($type == 'data') {
                $responseData = array(
                    'status' => true,
                    'message' => "success",
                    'data' => $data
                );
            }
            elseif ($type == 'noRecord') {
                 $responseData = array(
                    'status' => false,
                    'data' => $data,
                    'message' => "No Record Found"
                );
             } 
            else {
                $responseData = array(
                    'status' => 'true',
                    'data' => array(),
                    'message' => $data
                );
            }
            $responseData = array_merge($responseData, $optional);
        } else {
            $responseData = array(
                'status' => '-1',
                'message' => $data
            );
        }
        return $responseData;
    }

    public function login(Request $request)
    {
    	// dd($request);
        $inputs = $request->all();
        $validator = Validator::make(request()->all(), [
            'email' => 'required',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse(false, 'data', $validator->messages()->first());
        }
        // $token = $this->generateToken();
        try {
            $loginCredentials = [
                'email' => $inputs['email'],
                'password' => $inputs['password'],
            ];
            if (Auth::attempt($loginCredentials)) {
                $user = Auth::user();
                // $userToken = $this->saveToken($user->id,$token,'login');
                $userDetails = User::where('id',$user->id)->first();
                // $userDetails->token = $token;
                if(isset($inputs['device_token']))
                {
                    // $userFCMToken = $this->saveFCMToken($user->id,$inputs['device_token'],'login');
                }
                $duties = Duty::where('driver_id',$userDetails['id'])->with('vehicle')->with('driver')->with('user')->with('dropoff')->get();
                return response()->json(['status' => '1' ,'message' => 'Login Successfull' ,'data' => $duties]);

            }
            else
            {
                return response()->json(['status' => '0' ,'message' => 'Incorrect email or password, Please Try again.']);
            }
        } catch (Exception $ex) {
            return response()->json(['status' => '0' ,'message' => 'Incorrect email or password, Please Try again.']);
        }
    }
}
