<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use InterventionImage;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('prevent-back-history');
    }
    public function index()
    {
        return view('profile');
    }

    public function profile_update_detail(Request $request)
    {
        User::whereId(Auth::id())->update([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'mobile' => $request->get('mobile'),
            'city' => $request->get('city'),
            'dob' => $request->get('dob'),
            'about' => $request->get('about')

        ]);
        $notification = array(
            'message' => 'Personal Information Updated successfully !',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }

    public function profile_update_avatar(Request $request)
    {
        $inputs = $request->all();
        $user_id = Auth::user()->id;
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $orignalName = $file->getClientOriginalName();
            $destinationPath = public_path() . '/storage/avatar/'; // upload path
            $unique_name = md5($orignalName . time()) . '.' . $file->getClientOriginalExtension();
            $inputs['avatar'] = $unique_name;
            $image = InterventionImage::make($file);
            $background = InterventionImage::canvas($image->width(), $image->height());
            $background->insert($image, 'center');
            $background->save($destinationPath.''.$unique_name,100);
        }
        $user = User::find($user_id);
        $user->avatar = $unique_name;
        $user->save();
        $notification = array(
            'message' => 'Profile Picture Updated successfully !',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }

    public function profile_update_password(Request $request)
    {

    }
}
