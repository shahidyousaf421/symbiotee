<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use function Sodium\compare;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('prevent-back-history');
    }
    
    public function create_user_view()
    {
        return view('pages.admin.user.create_user');
    }

    public function create_user(Request $request)
    {
        if (User::whereEmail($request->get('email'))->first() != null) {
            $notification = array(
                'message' => 'Email Address Already Taken.',
                'alert-type' => 'info'
            );
            return back()->with($notification);
        } elseif ($request->get('name') == null) {
            $notification = array(
                'message' => 'Name Field Cannot be Null.',
                'alert-type' => 'info'
            );
            return back()->with($notification);
        } elseif ($request->get('email') == null) {
            $notification = array(
                'message' => 'Email Field Cannot be Null.',
                'alert-type' => 'info'
            );
            return back()->with($notification);
        } elseif ($request->get('user_type') == null) {
            $notification = array(
                'message' => 'Please Select User Type.',
                'alert-type' => 'info'
            );
            return back()->with($notification);
        }

        $samplePass = '123456';
        $user = User::newModelInstance($request->all());
        $user->password = bcrypt($samplePass);
        $user->save();

        $notification = array(
            'message' => 'User Created Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }

    public function edit_user_view(Request $request)
    {
        $user = User::whereId($request->get('id'))->first();
        return view('pages.admin.user.edit_user',compact('user'));
    }

    public function edit_user(Request $request)
    {
        User::whereId($request->get('id'))->update([
            'name' => $request['name'],
            'email' => $request['email'],
            'user_type' => $request['user_type']
        ]);

        $all_users = User::where('user_type', 'Admin')
            ->orWhere('user_type', 'Manager')
            ->orWhere('user_type', 'Customer')
            ->get();

        $notification = array(
            'message' => 'User Updated Successfully',
            'alert-type' => 'success'
        );
        return view('pages.admin.user.all_user',compact('all_users','notification'));
    }

    public function all_user()
    {
        $all_users = User::where('user_type', 'Admin')
            ->orWhere('user_type', 'Manager')
            ->orWhere('user_type', 'Customer')
            ->get();
        return view('pages.admin.user.all_user', compact('all_users'));
    }

    public function delete_user(Request $request)
    {
        $user = User::find($request->get('id'));
        $user->delete();

        $notification = array(
            'message' => 'User Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }

    public function pas_rest_user(Request $request)
    {
        User::whereId($request->get('id'))->update([
            'password' => bcrypt('123456')
        ]);
        $notification = array(
            'message' => 'User Password Reset Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }
}
