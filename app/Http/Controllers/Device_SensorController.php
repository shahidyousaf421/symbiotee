<?php

namespace App\Http\Controllers;

use App\Device_Sensor;
use App\Device_Type;
use Illuminate\Http\Request;

class Device_SensorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('prevent-back-history');
    }
    public function create_sensor_view()
    {
        $device_types = Device_Type::all();
        return view('pages.admin.device_sensor.create_sensor',compact('device_types'));
    }

    public function create_sensor(Request $request)
    {
        $device = Device_Sensor::newModelInstance($request->all());
        $device->save();

        $notification = array(
            'message' => 'Device Sensor Created Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }

    public function all_sensor()
    {
        $all_sensors = Device_Sensor::with('device_type')->get();
        return view('pages.admin.device_sensor.all_sensors', compact('all_sensors'));
    }

    public function edit_sensor_view(Request $request)
    {
        $sensor = Device_Sensor::whereId($request->get('id'))->with('device_type')->first();
        $device_types = Device_Type::all();
        return view('pages.admin.device_sensor.edit_sensor', compact('sensor','device_types'));
    }

    public function delete_sensor(Request $request)
    {
        $sensor = Device_Sensor::find($request->get('id'));
        $sensor->delete();

        $notification = array(
            'message' => 'Device Sensor Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }

    public function edit_sensor(Request $request)
    {
        Device_Sensor::whereId($request->get('id'))->update([
            'name' => $request['name'],
            'device_type_id' => $request['device_type_id'],
            'sensor_id' => $request['sensor_id'],
            'description' => $request['description'],
            'note' => $request['note']
        ]);

        $all_sensors = Device_Sensor::with('device_type')->get();

        $notification = array(
            'message' => 'Device Sensor Updated Successfully',
            'alert-type' => 'success'
        );

        return redirect('/Device/Sensor/all')->with($notification,$all_sensors);
    }
}
