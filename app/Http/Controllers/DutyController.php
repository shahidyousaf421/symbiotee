<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Auth;
use App\Order;
use App\User;
use App\Vehicle;
use App\Planned_Route;
use App\Planned_dropoff;
use App\Duty;
use Mapper;
use GMaps;

class DutyController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('prevent-back-history');
    }
    public function create_duty_view()
    {
    	$orders = Order::with('drop_location')->whereNotIn('id',Planned_dropoff::select('order_id')->get())->get();
        // dd($orders);
        $drivers = User::where('user_type','=','Driver')->get();
        $vehicles = Vehicle::all();
        return view('pages.manager.create_duty',compact('orders','drivers','vehicles'));
    }

    public function create_duty(Request $request)
    {
        $inputs = $request->all();

        $plan_route = Planned_Route::newModelInstance();
        $plan_route->location_id = $inputs['stop_location'];
        $plan_route->min_stop = $inputs['min_stop'];
        $plan_route->max_stop = $inputs['max_stop'];
        $plan_route->planned_stop_id = 1;
        $plan_route->stop_duration = $inputs['stop_duration'];
        $plan_route->save();

        $plan_route_id = Planned_Route::select('id')->max('id');

        $duty = Duty::newModelInstance();
        $duty->user_id = Auth::id();
        $duty->vehicle_id = $inputs['truck'];
        $duty->driver_id = $inputs['driver'];
        $duty->planned_route_id = $plan_route_id;
        $duty->departure = $inputs['departure_date'];
        $duty->journey_start_time = $inputs['departure_date'];
        $duty->status = 'New';
        $duty->save();

        $duty_id = Duty::select('id')->max('id');

        $orders = explode(",",$inputs['planned_route']);

        foreach ($orders as $key => $value) {
            $plan_dropoff = Planned_dropoff::newModelInstance();
            $plan_dropoff->duty_id = $duty_id;
            $plan_dropoff->order_id = $value;
            $plan_dropoff->save();
        }
        $notification = array(
            'message' => 'Duty Created Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }

    public function all_duty()
    {
        $all_duties = Duty::with('vehicle')->with('driver')->with('user')->with('dropoff')->with('planned_route')->get();
        // dd($all_duties);
        return view('pages.manager.all_duty',compact('all_duties'));
    }

    public function edit_duty_view(Request $request)
    {
        $duty = Duty::whereId($request->get('id'))->with('vehicle')->with('driver')->with('user')->with('dropoff')->with('planned_route')->first();
        // dd($duty);
        $orders = Order::with('drop_location')->whereNotIn('id',Planned_dropoff::select('order_id')->get())->get();
        $drivers = User::where('user_type','=','Driver')->get();
        // dd($drivers);
        $vehicles = Vehicle::all();
        return view('pages.manager.edit_duty',compact('orders','drivers','vehicles','duty'));
    }

    public function edit_duty(Request $request)
    {
        // dd($request);
        $inputs = $request->all();

        Planned_Route::newModelInstance()->update([
            'location_id' => $inputs['stop_location'],
            'min_stop' => $inputs['min_stop'],
            'max_stop' => $inputs['max_stop'],
            'stop_duration' => $inputs['stop_duration']
        ]);

        Duty::newModelInstance()->update([
            'vehicle_id' => $inputs['truck'],
            'driver_id' => $inputs['driver'],
            'departure' => $inputs['departure_date']
        ]);

        $dropoff = Planned_dropoff::where('duty_id',$request->get('id'))->delete();

        $orders = explode(",",$inputs['planned_route']);

        // dd($orders);

        foreach ($orders as $key => $value) {
            $plan_dropoff = Planned_dropoff::newModelInstance();
            $plan_dropoff->duty_id = $request->get('id');
            $plan_dropoff->order_id = $value;
            $plan_dropoff->save();
        }
        $notification = array(
            'message' => 'Duty Created Successfully',
            'alert-type' => 'success'
        );

        $all_duties = Duty::with('vehicle')->with('driver')->with('user')->with('dropoff')->with('planned_route')->get();
        return view('pages.manager.all_duty',compact('all_duties'));
    }

    public function delete_duty(Request $request)
    {
        $duty = Duty::whereId($request->get('id'))->first();
        $rout = Planned_Route::whereId($duty->planned_route_id)->first();
        $dropoff = Planned_dropoff::where('duty_id',$duty->id)->delete();
        $rout->delete();
        $duty->delete();

        $notification = array(
            'message' => 'Duty Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }

    public function track_duty_view()
    {
        $duties = Duty::all(); 


        // $config['center'] = '48.855510,2.365505';
        $config['center'] = 'auto';
        GMaps::initialize($config);
        $map = GMaps::create_map();
        return view('pages.manager.track_duty',compact('map','duties'));
    }

    public function track_duty()
    {
        $duties = Duty::all(); 


        // $config['center'] = '48.855510,2.365505';
        $config['center'] = 'auto';
        $config['directions'] = true;
        $config['directionsMode'] = "DRIVING";

        $config['directionsStart'] = '14 RUE GRANDE, 36170 Saint-Benoît-du-Sault, France';
        $config['directionsWaypointArray'] = array(
        "16 Place Jean Jaurès, 33220 Sainte-Foy-la-Grande, France",
        "33 RUE ROUVIER C.CIAL SUPER ITM MERCOEUR, 63100 Clermont-Ferrand, France"
        );
        $config['directionsEnd'] = '18 RUE IRENEE CARRE 08000 - CHARLEVILLE MEZIERES';
        $config['directionsDivID'] =  'directionsDiv';
        GMaps::initialize($config);
        $map = GMaps::create_map();
        return view('pages.manager.track_duty',compact('map','duties'));
    }
}
