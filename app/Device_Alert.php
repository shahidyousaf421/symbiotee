<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device_Alert extends Model
{
    protected $fillable = [
        'alert_collection_id',
        'order_id',
        'device_id',
        'alert_type_id',
        'sensor_name',
        'sensor_value'
    ];
}
