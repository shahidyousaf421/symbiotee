<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = [
        'company_id',
        'location_type',
        'location_category',
        'dropoff_address',
        'name',
        'description',
        'opening_hours',
        'website',
        'longitude',
        'longitude_type',
        'latitude',
        'latitude_type'
    ];

    public function order_pick()
    {
        return $this->hasMany(Order::class);
    }

    public function order_drop()
    {
        return $this->hasMany(Order::class);
    }
}
