<?php $user_type = \Illuminate\Support\Facades\Auth::user()->user_type; ?>
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item">
                <a href="#" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="arrow"></span>
                    <span class="selected"></span>
                </a>
            </li>
            @if($user_type == 'Admin')
                <li class="nav-item">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="icon-users"></i>
                        <span class="title">Manage Users</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item">
                            <a href="{!! route('create-user-view') !!}" class="nav-link ">
                                <i class="fa fa-user-plus"></i>
                                <span class="title">Create User</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="{!! route('all-user') !!}" class="nav-link ">
                                <i class="fa fa-edit"></i>
                                <span class="title">Edit User</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-truck"></i>
                        <span class="title">Manage Vehicles</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item">
                            <a href="{!! route('create-vehicle-view') !!}" class="nav-link ">
                                <i class="fa fa-ambulance"></i>
                                <span class="title">Create Vehicle</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="{!! route('all-vehicle') !!}" class="nav-link ">
                                <i class="fa fa-edit"></i>
                                <span class="title">Edit Vehicle</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-floppy-o"></i>
                        <span class="title">Manage Device Services</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item">
                            <a href="{!! route('create-device-view') !!}" class="nav-link ">
                                <i class="fa fa-plus"></i>
                                <span class="title">Create Device Services</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="{!! route('all-device') !!}" class="nav-link ">
                                <i class="fa fa-edit"></i>
                                <span class="title">Edit Device Services</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-floppy-o"></i>
                        <span class="title">Manage Device Sensor</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item">
                            <a href="{!! route('create-sensor-view') !!}" class="nav-link ">
                                <i class="fa fa-plus"></i>
                                <span class="title">Create Device Sensor</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="{!! route('all-sensor') !!}" class="nav-link ">
                                <i class="fa fa-edit"></i>
                                <span class="title">Edit Device Sensor</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-map-marker"></i>
                        <span class="title">Manage Dropff_Locations</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item">
                            <a href="{!! route('create-location-view') !!}" class="nav-link ">
                                <i class="fa fa-plus"></i>
                                <span class="title">Create Dropff_Locations</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="#" class="nav-link ">
                                <i class="fa fa-edit"></i>
                                <span class="title">Edit Dropff_Locations</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-bell"></i>
                        <span class="title">Manage Alert</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item">
                            <a href="{!! route('create-alert-view') !!}" class="nav-link ">
                                <i class="fa fa-plus"></i>
                                <span class="title">Create Alert</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="{!! route('all-alert') !!}" class="nav-link ">
                                <i class="fa fa-edit"></i>
                                <span class="title">Edit Alert</span>
                            </a>
                        </li>
                    </ul>
                </li>
            @elseif($user_type == 'Manager')
                <li class="nav-item">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-cart-arrow-down"></i>
                        <span class="title">Manage Duty Rosters</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item">
                            <a href="{{ route('create-duty-view') }}" class="nav-link ">
                                <i class="fa fa-cart-plus"></i>
                                <span class="title">Create Duty Roster</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="{{ route('all-duty') }}" class="nav-link ">
                                <i class="fa fa-edit"></i>
                                <span class="title">Edit Duty Roster</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{ route('track-duty-view') }}" class="nav-link nav-toggle">
                        <i class="fa fa-map-marker"></i>
                        <span class="title">Track Duty Roster</span>
                        <span class="arrow"></span>
                        <span class="selected"></span>
                    </a>
                </li>
            @elseif($user_type == 'Customer')
                <li class="nav-item" {{ (route('create-order-view') == request()->url()) ? ' active open' : '' }}>
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-cart-arrow-down"></i>
                        <span class="title">Manage Orders</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item">
                            <a href="{!! route('create-order-view') !!}" class="nav-link ">
                                <i class="fa fa-cart-plus"></i>
                                <span class="title">Create Order</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="{!! route('all-order') !!}" class="nav-link ">
                                <i class="fa fa-edit"></i>
                                <span class="title">Edit Order</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item" {{ (route('track-order-view') == request()->url()) ? ' active open' : '' }}>
                    <a href="{{ route('track-order-view') }}" class="nav-link nav-toggle">
                        <i class="fa fa-map-marker"></i>
                        <span class="title">Track Order</span>
                        <span class="arrow"></span>
                        <span class="selected"></span>
                    </a>
                </li>
            @endif
        </ul>
    </div>
</div>
