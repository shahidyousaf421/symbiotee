@extends('master')

@section('PAGE_LEVEL')
    <link href="{{ asset('public/assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('css')
    <style>
        .form-wizard .steps {
            padding: 0px !important;
        }
    </style>
@endsection

@section('content')
    <h1 class="page-title"> Edit Order
        {{--<small>Edit Order</small>--}}
    </h1>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="#">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <i class="icon-users"></i>
                <a href="{!! route('all-order') !!}">Manage Orders</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>Edit Order</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light " id="form_wizard_1">
                <div class="portlet-body form">
                    <form class="form-horizontal" action="{!! route('edit-order',['id'=>$order->id,'a_id'=>$order->alert_collection_id]) !!}" id="submit_form" method="POST">
                        {!! csrf_field() !!}
                        <div class="form-wizard">
                            <div class="form-body">
                                <ul class="nav nav-pills nav-justified steps">
                                    <li>
                                        <a href="#tab1" data-toggle="tab" class="step active">
                                            <span class="number"> 1 </span>
                                            <span class="desc">
                                                <i class="fa fa-check"></i> Order Setup
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab2" data-toggle="tab" class="step">
                                            <span class="number"> 2 </span>
                                            <span class="desc">
                                                <i class="fa fa-check"></i> Alert Setup
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab4" data-toggle="tab" class="step">
                                            <span class="number"> 3 </span>
                                            <span class="desc">
                                                <i class="fa fa-check"></i> Confirm
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                                <div id="bar" class="progress progress-striped" role="progressbar">
                                    <div class="progress-bar progress-bar-success"> </div>
                                </div>
                                <div class="tab-content">
                                    <div class="alert alert-danger display-none">
                                        <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. </div>
                                    <div class="alert alert-success display-none">
                                        <button class="close" data-dismiss="alert"></button> Your form validation is successful! </div>
                                    <div class="tab-pane active" id="tab1">
                                        <h3 class="block">Order Information</h3>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Order Type
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <select name="order_type" class="form-control" required>
                                                    <option value="0" disabled selected>Select Order Type</option>
                                                    <option <?php if($order->order_type=='Container'){ ?> selected="selected" <?php }?> value="Container">Container</option>
                                                    <option <?php if($order->order_type=='Parcel'){ ?> selected="selected" <?php }?> value="Parcel">Parcel</option>
                                                    <option <?php if($order->order_type=='Pallet'){ ?> selected="selected" <?php }?> value="Pallet">Pallet</option>
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Transport Company
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <select name="company_id" class="form-control" required>
                                                    <option value="0" disabled selected>Select Company</option>
                                                    @foreach($companies as $company)
                                                    <option <?php if($order->company_id== $company->id){ ?> selected="selected" <?php }?> value="{{$company->id}}">{{$company->name}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Pickup Location
                                                <!-- <input type="radio" name="p_location" id="p_location_s" value="" data-title="Male" checked/> -->
                                            </label>
                                            <div class="col-md-4">
                                                <select name="pickup_location_id" class="form-control" required>
                                                    <option value="0" disabled selected>Select Location</option>
                                                    @foreach($locations as $location)
                                                    <option <?php if($order->pickup_location== $location->id){ ?> selected="selected" <?php }?> value="{{$location->id}}">{{$location->name}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <!-- <div class="form-group">
                                            <label class="control-label col-md-3">
                                                <input type="radio" name="p_location" id="p_location_p" value="" />
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="pickup_location_id"/>
                                                <span class="help-block"> Location Same As In Profile </span>
                                            </div>
                                        </div> -->
                                        <!-- <div class="form-group">
                                            <label class="control-label col-md-3">
                                                <input type="radio" name="p_location" id="p_location_d" value="" />
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="pickup_location_id" readonly="readonly" />
                                                <span class="help-block"> Location Different From Profile </span>
                                            </div>
                                        </div> -->
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Drop Location
                                                <!-- <input type="radio" name="d_location" id="d_location_s" value="" data-title="Male" checked/> -->
                                            </label>
                                            <div class="col-md-4">
                                                <select name="dropoff_location_id" class="form-control" required>
                                                    <option value="0" disabled selected>Select Location</option>
                                                    @foreach($locations as $location)
                                                    <option <?php if($order->dropoff_location== $location->id){ ?> selected="selected" <?php }?> value="{{$location->id}}">{{$location->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <!-- <div class="form-group">
                                            <label class="control-label col-md-3">
                                                <input type="radio" name="d_location" id="d_location_d" value="" />
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="dropoff_location_id" readonly="readonly" />
                                                <span class="help-block"> Location Different From Profile </span>
                                            </div>
                                        </div> -->
                                    </div>
                                    <div class="tab-pane" id="tab2">
                                        <h3 class="block">Alert Information</h3>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Alert Type</label>
                                            <div class="col-md-4">
                                                <select name="alert_type" class="form-control" required>
                                                    <option value="0" disabled selected>Select Alert Type</option>
                                                    @foreach($alerts as $alert)
                                                        <option <?php if($order->alert_collection->alert_type_id== $alert->id){ ?> selected="selected" <?php }?> value="{{$alert->id}}">{{$alert->name}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Condition</label>
                                            <div class="col-md-4">
                                                <select name="alert_condition" class="form-control" required>
                                                    <option value="0" disabled selected>Select Condition</option>
                                                    <option <?php if($order->alert_collection->condition== '<'){ ?> selected="selected" <?php }?> value="<"> < </option>
                                                    <option <?php if($order->alert_collection->condition== '>'){ ?> selected="selected" <?php }?> value=">"> > </option>
                                                    <option <?php if($order->alert_collection->condition== '<='){ ?> selected="selected" <?php }?> value="<="> <= </option>
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Value</label>
                                            <div class="col-md-4">
                                                <input type="text" value="{{ $order->alert_collection->value }}" class="form-control" name="alert_value" />
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab4">
                                        <h3 class="block">Confirm your account</h3>
                                        <h4 class="form-section">Order Information</h4>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Order Type:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="order_type"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Transport Company:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="company_id"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Pickup Location:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="pickup_location_id"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Drop Location:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="dropoff_location_id"> </p>
                                            </div>
                                        </div>
                                        <h4 class="form-section">Alert Information</h4>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Alert Type:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="alert_type"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Condition:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="alert_condition"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Value:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="alert_value"> </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <a href="javascript:;" class="btn default button-previous">
                                            <i class="fa fa-angle-left"></i> Back </a>
                                        <a href="javascript:;" class="btn btn-outline green button-next"> Continue
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                        <a href="javascript:;">
                                            <input type="submit" class="btn green button-submit" value="Submit">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('PAGE_LEVEL_PLUGIN')
    <script src="{{ asset('public//assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{!! asset('public/assets/global/plugins/jquery-validation/js/additional-methods.min.js') !!}" type="text/javascript"></script>
    <script src="{{ asset('public/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}" type="text/javascript"></script>
@endsection


@section('PAGE_LEVEL_SCRIPT')
    <script src="{{ asset('public/assets/pages/scripts/form-wizard.min.js') }}" type="text/javascript"></script>
@endsection

@section('js')
@endsection