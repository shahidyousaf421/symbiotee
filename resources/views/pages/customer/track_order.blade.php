@extends('master')

@section('PAGE_LEVEL')
@endsection

@section('css')
@endsection

@section('content')
    <h1 class="page-title"> Track Order
        <small>Track Your Order</small>
    </h1>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="#">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>Track Order</span>
            </li>
        </ul>
    </div>
    <div class="row">
        <!-- BEGIN MARKERS PORTLET-->
        <div class="portlet light portlet-fit ">
            <div class="portlet-title">
                <div class="col-md-12">
                    <div class="col-md-3"ion ></div>
                    <div class="col-md-4">
                        <select name="order" id="order" class="form-control">
                            <option value="0" disabled selected>Select Order To Track</option>
                            @foreach($orders as $order)
                            <option <?php if($order->order_type==$single_order['order_type']){ ?> selected="selected" <?php }?> value="{{$order->id}}">{{$order->order_type}}</option>
                            @endforeach            
                        </select>
                    </div>
                    <div class="col-md-5">
                        <input type="button" onclick="get_route()" class="btn btn-primary" value="Search">
                    </div>
                </div>
                <div class="caption">
                    <i class=" icon-layers font-blue"></i>
                    <span class="caption-subject font-blue bold uppercase">Map</span>
                </div>
            </div>
            {!! $map['js'] !!}
            <div class="portlet-body">
                <div id="map">
                {!! $map['html'] !!}
                <!-- <div id="directionsDiv"></div> -->
            </div>
            </div>
        </div>
        <!-- END MARKERS PORTLET-->
    </div>
@endsection


@section('PAGE_LEVEL_PLUGIN')
@endsection


@section('PAGE_LEVEL_SCRIPT')
@endsection


@section('js')
    <script>
        function get_route() 
        {
            var sel = $('#order').children("option:selected").val();
            var base = '{!! route('track-order') !!}';
            var url = base+'?id='+sel ;
            // debugger;
            document.location.href=url;
            // $.ajax({
            //     type:'POST',
            //     url:"{{URL::route('track-order')}}",
            //     data: {item:sel},
            //     success:function(data){
            //     },
            //     error:function(){
            //     }
            // });
        }
    </script>
@endsection