@extends('master')

@section('PAGE_LEVEL')
    <link href="{{ asset('public/assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('css')
    <style>
        .form-wizard .steps {
            padding: 0px !important;
        }

        .facet-container{
          width: 330px;
        }
        .right {
          float: right;
        }
        .left {
          float: left;
        }
        p {
          clear: both;
          padding-top: 1em;
        }
        .facet-list {
          list-style-type: none;
          margin: 0;
          padding: 0;
          margin-right: 10px;
          background: #eee;
          padding: 5px;
          min-height: 1.5em;
          font-size: 0.85em;
        }
        .facet-list li {
          margin: 5px;
          padding: 5px;
          font-size: 1.2em;
        }
        .facet-list li.placeholder {
          height: 1.2em
        }
        .facet {
          border: 1px solid #bbb;
          background-color: #fafafa;
          cursor: move;
        }
        .facet.ui-sortable-helper {
          opacity: 0.5;
        }
        .placeholder {
          border: 1px solid orange;
          background-color: #fffffd;
        }
    </style>
@endsection

@section('content')
    <h1 class="page-title"> Create Duty
        <small>Duty Roster</small>
    </h1>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="#">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>Manage Duty Rosters</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light " id="form_wizard_1">
                <div class="portlet-body form">
                    <form class="form-horizontal" action="{!! route('create-duty') !!}" id="submit_form" method="POST">
                        {{ csrf_field() }}
                        <div class="form-wizard">
                            <div class="form-body">
                                <ul class="nav nav-pills nav-justified steps">
                                    <li>
                                        <a href="#tab1" data-toggle="tab" class="step active">
                                            <span class="number"> 1 </span>
                                            <span class="desc">
                                                <i class="fa fa-check"></i> Duty Setup
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab2" data-toggle="tab" class="step">
                                            <span class="number"> 2 </span>
                                            <span class="desc">
                                                <i class="fa fa-check"></i> journey Setup
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab4" data-toggle="tab" class="step">
                                            <span class="number"> 3 </span>
                                            <span class="desc">
                                                <i class="fa fa-check"></i> Confirm
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                                <div id="bar" class="progress progress-striped" role="progressbar">
                                    <div class="progress-bar progress-bar-success"> </div>
                                </div>
                                <div class="tab-content">
                                    <div class="alert alert-danger display-none">
                                        <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. </div>
                                    <div class="alert alert-success display-none">
                                        <button class="close" data-dismiss="alert"></button> Your form validation is successful! </div>
                                    <div class="tab-pane active" id="tab1">
                                        <h3 class="block">Duty Information</h3>
                                        <div class="row">      
                                            <div class="col-md-7">
                                                <div class="form-group">
                                                    <label class="control-label col-md-5">Drop-off Location</label>
                                                    <div class="col-md-7">
                                                        <ul id="allFacets" class="facet-list">
                                                            @foreach($orders as $order)
                                                            <li value="{{ $order->id }}" class="facet">{{ $order->drop_location->name }}</li>
                                                            @endforeach
                                                        </ul>

                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-5">Truck
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-7">
                                                        <select name="truck" class="form-control" required>
                                                            <option value="0" disabled selected>Select Order Type</option>
                                                            @foreach($vehicles as $vehicle)
                                                            <option value="{{ $vehicle->id }}">{{ $vehicle->name }}-{{ $vehicle->number }}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-5">Driver
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-7">
                                                        <select name="driver" class="form-control" required>
                                                            <option value="0" disabled selected>Select Driver</option>
                                                            @foreach($drivers as $driver)
                                                            <option value="{{ $driver->id }}">{{ $driver->name }}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-5">Planned Departure
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-7">
                                                        <input type="date" name="departure_date" class="form-control" required>
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>    
                                            </div>
                                            <div class="col-md-5">

                                                <label>Planned Route</label>
                                                <ul id="userFacets" class="facet-list plan">
                                                  
                                                </ul>
                                                <input type="hidden" id="planned_route" name="planned_route">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab2">
                                        <h3 class="block">Journey Information</h3>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Maximum Stops
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" name="max_stop" class="form-control" />
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Minimum Stops
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" name="min_stop" class="form-control" />
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Maximum Stop Duration
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" name="stop_duration" class="form-control" />
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Stops Location
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" name="stop_location" class="form-control" />
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab4">
                                        <h3 class="block">Confirm your account</h3>
                                        <h4 class="form-section">Duty Information</h4>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Drop-off Location:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="drop_location"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Truck:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="truck"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Driver:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="driver"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Planned Departure:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="departure_date"> </p>
                                            </div>
                                        </div>
                                        <h4 class="form-section">Journey Information</h4>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Device:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="device"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Category:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="category"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Condition:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="condition"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Value:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="value"> </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <a href="javascript:;" class="btn default button-previous">
                                            <i class="fa fa-angle-left"></i> Back </a>
                                        <a href="javascript:;" class="btn btn-outline green button-next"> Continue
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                        <a href="javascript:;">
                                            <input type="submit" class="btn green button-submit" value="Submit">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('PAGE_LEVEL_PLUGIN')
    <script src="{{ asset('public//assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{!! asset('public/assets/global/plugins/jquery-validation/js/additional-methods.min.js') !!}" type="text/javascript"></script>
    <script src="{{ asset('public/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}" type="text/javascript"></script>
@endsection


@section('PAGE_LEVEL_SCRIPT')
    <script src="{{ asset('public/assets/pages/scripts/form-wizard.min.js') }}" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
@endsection


@section('js')
    <script type="text/javascript">
        $( "#submit_form" ).submit(function( event ) {
            var plan_array = [];
            const listItems = document.querySelectorAll('.plan li');
            for (let i = 0; i < listItems.length; i++) {
              plan_array.push(listItems[i].value);
            }
            console.log(plan_array);
            $('#planned_route').val(plan_array);
        });
        $(function() {
            $("#allFacets, #userFacets").sortable({
                connectWith: "ul",
                placeholder: "placeholder",
                delay: 150
            })
            .disableSelection()
            .dblclick( function(e){
                var item = e.target;
                if (e.currentTarget.id === 'allFacets') {
                    //move from all to user
                    $(item).fadeOut('fast', function() {
                        $(item).appendTo($('#userFacets')).fadeIn('slow');
                    });
                } else {
                    //move from user to all
                    $(item).fadeOut('fast', function() {
                        $(item).appendTo($('#allFacets')).fadeIn('slow');
                    });
                }
            });
        });
    </script>
@endsection