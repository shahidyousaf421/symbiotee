@extends('master')

@section('PAGE_LEVEL')
@endsection

@section('css')
@endsection

@section('content')
    <h1 class="page-title"> Track Duty
        <small>Track Your Duty Roster</small>
    </h1>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="#">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>Track Duty Roster</span>
            </li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-fit ">
            <div class="portlet-title">
                <div class="col-md-12">
                    <div class="col-md-3"ion ></div>
                    <div class="col-md-4">
                        <select name="order" id="order" class="form-control">
                            <option value="0" disabled selected>Select Order To Track</option>
                            @foreach($duties as $duty)
                            <option value="{{$duty->id}}">{{$duty->id}}</option>
                            @endforeach            
                        </select>
                    </div>
                    <div class="col-md-5">
                        <input type="button" onclick="get_duty()" class="btn btn-primary" value="Search">
                    </div>
                </div>
                <div class="caption">
                    <i class=" icon-layers font-blue"></i>
                    <span class="caption-subject font-blue bold uppercase">Map</span>
                </div>
            </div>
            {!! $map['js'] !!}
            <div class="portlet-body">
                <div> {!! $map['html'] !!} </div>
            </div>
        </div>
        </div>
    </div>
@endsection


@section('PAGE_LEVEL_PLUGIN')
    <script src="{!! asset('public/assets/global/plugins/gmaps/gmaps.min.js') !!}" type="text/javascript"></script>
    
@endsection


@section('PAGE_LEVEL_SCRIPT')
    <script src="{!! asset('public/assets/pages/scripts/maps-google.min.js') !!}" type="text/javascript"></script>
@endsection


@section('js')
    <script>
        function get_duty() 
        {
            var sel = $('#order').children("option:selected").val();
            var base = '{!! route('track-duty') !!}';
            // var url = base+'?id='+id ;
            // debugger;
            document.location.href=base;
            // $.ajax({
            //     type:'POST',
            //     url:"{{URL::route('track-order')}}",
            //     data: {item:sel},
            //     success:function(data){
            //     },
            //     error:function(){
            //     }
            // });
        }
    </script>
@endsection