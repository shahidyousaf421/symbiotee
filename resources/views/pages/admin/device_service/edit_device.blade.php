@extends('master')

@section('PAGE_LEVEL')
    <link href="{{ asset('public/assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('css')
    <style>
        .form-wizard .steps {
            padding: 0px !important;
        }
    </style>
@endsection

@section('content')
    <h1 class="page-title"> Edit Device Service
        {{--<small>Edit New User</small>--}}
    </h1>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="#">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <i class="icon-users"></i>
                <a href="{!! route('all-device') !!}">Manage Device Services</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>Edit Device Service</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light " id="form_wizard_1">
                <div class="portlet-body form">
                    <form class="form-horizontal" action="{!! route('edit-device',['id'=>$device->id]) !!}" id="submit_form" method="POST">
                        {!! csrf_field() !!}
                        <div class="form-wizard">
                            <div class="form-body">
                                <ul class="nav nav-pills nav-justified steps">
                                    <li>
                                        <a href="#tab1" data-toggle="tab" class="step active">
                                            <span class="number"> 1 </span>
                                            <span class="desc">
                                                <i class="fa fa-check"></i> Device Setup
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab4" data-toggle="tab" class="step">
                                            <span class="number"> 2 </span>
                                            <span class="desc">
                                                <i class="fa fa-check"></i> Confirm
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                                <div id="bar" class="progress progress-striped" role="progressbar">
                                    <div class="progress-bar progress-bar-success"> </div>
                                </div>
                                <div class="tab-content">
                                    <div class="alert alert-danger display-none">
                                        <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. </div>
                                    <div class="alert alert-success display-none">
                                        <button class="close" data-dismiss="alert"></button> Your form validation is successful! </div>
                                    <div class="tab-pane active" id="tab1">
                                        <h3 class="block">Device Service Information</h3>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Name
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" value="{{$device->name}}" name="name" required/>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Device Type
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <select name="device_type_id" class="form-control" required>
                                                    <option value="0" disabled selected>Select Device Type</option>
                                                    @foreach($device_types as $device_type)
                                                        <option <?php if($device_type->id == $device->device_type->id){ ?> selected="selected" <?php }?> value="{{$device_type->id}}">{{$device_type->name}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Company
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <select name="company_id" class="form-control" required>
                                                    <option value="0" disabled selected>Select Company</option>
                                                    @foreach($companies as $company)
                                                        <option <?php if($company->id == $device->company->id){ ?> selected="selected" <?php }?> value="{{$company->id}}">{{$company->name}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Status
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <select name="status" class="form-control" required>
                                                    <option value="0" disabled selected>Select Status</option>
                                                    <option <?php if($device->status == 'active'){ ?> selected="selected" <?php }?> value="active">Active</option>
                                                    <option <?php if($device->status == 'de-active'){ ?> selected="selected" <?php }?> value="de-active">De-Active</option>
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Lease Start Date
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="date" class="form-control" value="{{$device->lease_start}}" name="lease_start" required/>
                                                <span class="help-block"> </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Lease End Date
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="date" class="form-control" value="{{$device->lease_end}}" name="lease_end" required/>
                                                <span class="help-block"> </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Frequency
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" value="{{$device->frequency}}" name="frequency" required/>
                                                <span class="help-block"> </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Sampling
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" value="{{$device->sampling}}" name="sampling" required/>
                                                <span class="help-block"> </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab4">
                                        <h3 class="block">Confirm your Devices Service</h3>
                                        <h4 class="form-section">Device Information</h4>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Name:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="name"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Status:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="status"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Lease Start Date:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="lease_start"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Lease End Date:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="lease_end"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Frequency:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="frequency"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Sampling:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="sampling"> </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <a href="javascript:;" class="btn default button-previous">
                                            <i class="fa fa-angle-left"></i> Back </a>
                                        <a href="javascript:;" class="btn btn-outline green button-next"> Continue
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                        <a href="javascript:;">
                                            <input type="submit" class="btn green button-submit" value="Submit">
                                            {{--<input type="submit" class="btn green" value="Submit">--}}
                                            {{--<i class="fa fa-check"></i>--}}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('PAGE_LEVEL_PLUGIN')
    <script src="{{ asset('public//assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{!! asset('public/assets/global/plugins/jquery-validation/js/additional-methods.min.js') !!}" type="text/javascript"></script>
    <script src="{{ asset('public/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}" type="text/javascript"></script>
@endsection


@section('PAGE_LEVEL_SCRIPT')
    <script src="{{ asset('public/assets/pages/scripts/form-wizard.min.js') }}" type="text/javascript"></script>
@endsection

@section('js')
@endsection