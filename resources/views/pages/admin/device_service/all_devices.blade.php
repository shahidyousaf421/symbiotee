@extends('master')

@section('PAGE_LEVEL')

@endsection

@section('css')
    <style>
        .css-serial {
            counter-reset: serial-number;  /* Set the serial number counter to 0 */
        }

        .css-serial td:first-child:before {
            counter-increment: serial-number;  /* Increment the serial number counter */
            content: counter(serial-number);  /* Display the counter */
        }
    </style>
@endsection

@section('content')
    <h1 class="page-title"> All Device Services
        {{--<small>Edit New User</small>--}}
    </h1>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="#">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>Manage Device Services</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-fit portlet-datatable ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-users"></i>
                        <span class="caption-subject bold uppercase">All Device Services</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                                <i class="fa fa-share"></i>
                                <span class="hidden-xs"> Trigger Tools </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right" id="sample_3_tools">
                                <li>
                                    <a href="javascript:;" data-action="0" class="tool-action">
                                        <i class="icon-printer"></i> Print</a>
                                </li>
                                <li>
                                    <a href="javascript:;" data-action="1" class="tool-action">
                                        <i class="icon-check"></i> Copy</a>
                                </li>
                                <li>
                                    <a href="javascript:;" data-action="2" class="tool-action">
                                        <i class="icon-doc"></i> PDF</a>
                                </li>
                                <li>
                                    <a href="javascript:;" data-action="3" class="tool-action">
                                        <i class="icon-paper-clip"></i> Excel</a>
                                </li>
                                <li>
                                    <a href="javascript:;" data-action="4" class="tool-action">
                                        <i class="icon-cloud-upload"></i> CSV</a>
                                </li>
                                <li class="divider"> </li>
                                <li>
                                    <a href="javascript:;" data-action="5" class="tool-action">
                                        <i class="icon-refresh"></i> Reload</a>
                                </li>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        <table class="table css-serial table-striped table-bordered table-hover" id="sample_3">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Name</th>
                                <th>Device Type</th>
                                <th>Company</th>
                                <th>Lease Start</th>
                                <th>Lease End</th>
                                <th>Frequency</th>
                                <th>Sampling</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($all_devices as $device)
                                <tr>
                                    <td></td>
                                    <td> {!! $device->name !!} </td>
                                    <td> {!! $device->device_type->name !!} </td>
                                    <td> {!! $device->company->name !!} </td>
                                    <td> {!! $device->lease_start !!} </td>
                                    <td> {!! $device->lease_end !!} </td>
                                    <td> {!! $device->frequency !!} </td>
                                    <td> {!! $device->sampling !!} </td>
                                    <td> {!! $device->status !!} </td>
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn btn-xs red btn-outline btn-circle dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-left" role="menu">
                                                <li>
                                                    <a href="{!! route('edit-device-view',['id' => $device->id] ) !!}">
                                                        <i class="icon-pencil"></i>Edit</a>
                                                </li>
                                                <li>
                                                    <a href="{!! route('delete-device',['id' => $device->id] ) !!}" class="">
                                                        <i class="icon-trash"></i> Delete</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('PAGE_LEVEL_PLUGIN')
    <script src="{{asset('public/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
@endsection


@section('PAGE_LEVEL_SCRIPT')
    <script src="{{asset('public/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/pages/scripts/table-datatables-buttons.min.js')}}" type="text/javascript"></script>
@endsection

@section('js')
@endsection