@extends('master')

@section('PAGE_LEVEL')
@endsection

@section('css')
    <style type="text/css">
        .mapControls {
            margin-top: 10px;
            border: 1px solid transparent;
            border-radius: 2px 0 0 2px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            height: 32px;
            outline: none;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        }
        #searchMapInput {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 50%;
        }
        #searchMapInput:focus {
            border-color: #4d90fe;
        }
    </style>
@endsection

@section('content')
    <h1 class="page-title"> Add Location
        <small>Add New Location</small>
    </h1>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="#">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>Manage Location</span>
            </li>
        </ul>
    </div>
    <div class="row">
        <!-- BEGIN MARKERS PORTLET-->
        <div class="portlet light portlet-fit ">
            <div class="portlet-title">
                <input id="searchMapInput" class="mapControls" type="text" placeholder="Enter a location">
                <ul id="geoData">
                    <li>Full Address: <span id="location-snap"></span></li>
                    <li>Latitude: <span id="lat-span"></span></li>
                    <li>Longitude: <span id="lon-span"></span></li>
                </ul>
                <div class="caption">
                    <i class=" icon-layers font-blue"></i>
                    <span class="caption-subject font-blue bold uppercase">Map</span>
                </div>
            </div>
            <div class="portlet-body">
                <div id="map">
                
                </div>
            </div>
        </div>
        <!-- END MARKERS PORTLET-->
    </div>
@endsection


@section('PAGE_LEVEL_PLUGIN')
    
@endsection


@section('PAGE_LEVEL_SCRIPT')
    <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDIfwqc5yMbC6Y1bvfJEQkun_MpK8krvIY&libraries=places&callback=initMap" async defer></script> -->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAtqWsq5Ai3GYv6dSa6311tZiYKlbYT4mw&libraries=places&callback=initMap"
  type="text/javascript"></script>
    
@endsection


@section('js')
    <script>
        function initMap() {
            var input = document.getElementById('searchMapInput');
          
            var autocomplete = new google.maps.places.Autocomplete(input);
           
            autocomplete.addListener('place_changed', function() {
                var place = autocomplete.getPlace();
                document.getElementById('location-snap').innerHTML = place.formatted_address;
                document.getElementById('lat-span').innerHTML = place.geometry.location.lat();

         
                document.getElementById('lon-span').innerHTML = place.geometry.location.lng();
            });
        }
    </script>
@endsection