{{--{!! dd($errors->first()) !!}--}}
@extends('layouts.login')

@section('PAGE_LEVEL')
    <link href="{!! asset('public/assets/global/plugins/select2/css/select2.min.css') !!}" rel="stylesheet" type="text/css"/>
    <link href="{!! asset('public/assets/global/plugins/select2/css/select2-bootstrap.min.css') !!}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('PAGE_LEVEL_STYLES')
    <link href="{!! asset('public/assets/pages/css/login.min.css') !!}" rel="stylesheet" type="text/css"/>
    <style>
        .login .content {
            margin: 0px auto 10px !important;
        }
    </style>
@endsection
@section('title','Login')

@section('content')

    <form class="form-horizontal login-form" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <div class="text-center"><h3 class="form-title">Sign In To You Account</h3></div>

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Email</label>
            <input style="color: #000;" id="email" class="form-control" type="text" autocomplete="off" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>

        {{--<div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">--}}
            {{--<label class="control-label visible-ie8 visible-ie9">Type</label>--}}
            {{--<select class="form-control" id="type" name="type">--}}
                {{--<option value="0" disabled selected>Select Type</option>--}}
                {{--<option value="customer">Customer</option>--}}
                {{--<option value="manager">Manager</option>--}}
                {{--<option value="admin">Admin</option>--}}
            {{--</select>--}}
            {{--@if ($errors->has('type'))--}}
                {{--<span class="help-block">--}}
                    {{--<strong>{{ $errors->first('type') }}</strong>--}}
                {{--</span>--}}
            {{--@endif--}}
        {{--</div>--}}

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <input style="color: #000;" id="password" class="form-control " type="password"
                   autocomplete="off" placeholder="Password" name="password" required>
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-actions">
            <button type="submit" class="btn green uppercase">Login</button>

            <label class="rememberme check mt-checkbox mt-checkbox-outline">
                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>Remember
                <span></span>
            </label>
        </div>
    </form>
@endsection

@section('PAGE_LEVEL_PLUGIN')
    <script src="{!! asset('public/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') !!}"
            type="text/javascript"></script>
    <script src="{!! asset('public/assets/global/plugins/jquery-validation/js/additional-methods.min.js') !!}"
            type="text/javascript"></script>
    <script src="{!! asset('public/assets/global/plugins/select2/js/select2.full.min.js') !!}" type="text/javascript"></script>
@endsection
@section('PAGE_LEVEL_SCRIPT')
    <script src="{!! asset('public/assets/pages/scripts/login.min.js') !!}" type="text/javascript"></script>
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $('#clickmewow').click(function () {
                $('#radio1003').attr('checked', 'checked');
            });
        })
    </script>
@endsection