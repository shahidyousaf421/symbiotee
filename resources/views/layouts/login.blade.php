<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{--<title>{{ config('app.name', 'Laravel') }}</title>--}}
    <title>@yield('title') - SYMBIOTEE </title>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="{!! asset("public/assets/global/css/open-sans.css") !!}" rel="stylesheet" type="text/css"/>
    <link href="{!! asset('public/assets/global/plugins/font-awesome/css/font-awesome.min.css') !!}" rel="stylesheet" type="text/css">
    <link href="{!! asset('public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') !!}" rel="stylesheet" type="text/css"/>
    <link href="{!! asset("public/assets/global/plugins/bootstrap/css/bootstrap.min.css") !!}" rel="stylesheet" type="text/css"/>
    <link href="{!! asset("public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css") !!}" rel="stylesheet" type="text/css"/>
    @yield('GLOBAL_MANDATORY')
<!-- END GLOBAL MANDATORY STYLES -->


    <!-- BEGIN PAGE LEVEL PLUGINS -->
    @yield('PAGE_LEVEL')
<!-- END PAGE LEVEL PLUGINS -->


    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{!! asset("public/assets/global/css/components.min.css") !!}" rel="stylesheet" type="text/css"/>
    <link href="{!! asset("public/assets/global/css/plugins.min.css") !!}" rel="stylesheet" type="text/css"/>
    @yield('THEME_GLOBAL')

<!-- END THEME GLOBAL STYLES -->


    <!-- BEGIN THEME LAYOUT STYLES -->
    @yield('THEME_LAYOUT')

<!-- END THEME LAYOUT STYLES -->


    <!-- BEGIN PAGE LEVEL STYLES -->
    @yield('PAGE_LEVEL_STYLES')
<!-- END PAGE LEVEL STYLES -->


    @yield('css')

    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<body class="login">


<div class="logo">
    <a href="">
        <img width="30%" class="ubookLoginLogo" src="{!! asset('public/assets/pages/img/login-logo.png') !!}" alt=""> </a>
</div>

<div class="content">
    @yield('content')
</div>


<div class="copyright"> 2019 © SYMBIOTEE. All Rights Reserved. </div>


<!-- BEGIN CORE PLUGINS -->
<script src="{!! asset("public/assets/global/plugins/jquery.min.js") !!}" type="text/javascript"></script>
<script src="{!! asset("public/assets/global/plugins/bootstrap/js/bootstrap.min.js") !!}" type="text/javascript"></script>
<script src="{!! asset("public/assets/global/plugins/js.cookie.min.js") !!}" type="text/javascript"></script>
<script src="{!! asset("public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js") !!}" type="text/javascript"></script>
<script src="{!! asset("public/assets/global/plugins/jquery.blockui.min.js") !!}" type="text/javascript"></script>
<script src="{!! asset("public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js") !!}" type="text/javascript"></script>
@yield('CORE')
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
@yield('PAGE_LEVEL_PLUGIN')
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="{!! asset("public/assets/global/scripts/app.min.js") !!}" type="text/javascript"></script>
@yield('THEME_GLOBAL')
<!-- END THEME GLOBAL SCRIPTS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
@yield('PAGE_LEVEL_SCRIPT')
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="{!! asset("public/assets/layouts/layout2/scripts/layout.min.js") !!}" type="text/javascript"></script>
<script src="{!! asset("public/assets/layouts/layout2/scripts/demo.min.js") !!}" type="text/javascript"></script>
<script src="{!! asset("public/assets/layouts/global/scripts/quick-sidebar.min.js") !!}" type="text/javascript"></script>
<script src="{!! asset("public/assets/layouts/global/scripts/quick-nav.min.js") !!}" type="text/javascript"></script>
<script src="{!! asset('public/assets/global/plugins/bootstrap-toastr/toastr.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset("public/js/app.js") !!}" type="text/javascript"></script>
@yield('THEME_LAYOUT')
<!-- END THEME LAYOUT SCRIPTS -->


@yield('js')


<script>
    $(document).ready(function () {
        $('#clickmewow').click(function () {
            $('#radio1003').attr('checked', 'checked');
        });
    })
</script>

<script>

            @if(Session::has('message'))


    var type = "{{Session::get('alert-type')}}";

    switch (type) {
        case "info":
            toastr.info("{{Session::get('message')}}");
            break;

        case "warning":
            toastr.warning("{{Session::get('message')}}");
            break;

        case "success":
            debugger;
            toastr.options.positionClass = "toast-top-left";
            toastr.options.fadeOut = 9500;
            toastr.success("{{Session::get('message')}}",{ fadeAway: 10000 });
            break;

        case "error":
            toastr.error("{{Session::get('message')}}");
            break;
    }
    @endif
</script>
</body>
</html>
