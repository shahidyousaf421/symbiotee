<?php $userdata = \Illuminate\Support\Facades\Auth::user();?>
@extends('master')

@section('PAGE_LEVEL')
    <link href="{!! asset('public/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') !!}" rel="stylesheet" type="text/css"/>
    <link href="{!! asset('public/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') !!}" rel="stylesheet" type="text/css"/>
    <link href="{!! asset('public/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') !!}" rel="stylesheet" type="text/css"/>
    <link href="{!! asset('public/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') !!}" rel="stylesheet" type="text/css"/>
    <link href="{!! asset('public/assets/global/plugins/clockface/css/clockface.css') !!}" rel="stylesheet" type="text/css"/>
    <link href="{!! asset('public/assets/pages/css/profile-2.min.css') !!}" rel="stylesheet" type="text/css"/>
    <link href="{!! asset('public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') !!}" rel="stylesheet" type="text/css" />
@endsection

@section('css')

@endsection

@section('content')
    <h1 class="page-title"> {{$userdata->user_type}}
        <small>Profile Page</small>
    </h1>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="#">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>Profile</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE HEADER-->
    <div class="profile">
        <div class="tabbable-line tabbable-full-width">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab_1_1" data-toggle="tab"> Overview </a>
                </li>
                <li>
                    <a id="accounttab" href="#tab_1_3" data-toggle="tab"> Account </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1_1">
                    <div class="row">
                        <div class="col-md-3">
                            <ul class="list-unstyled profile-nav">
                                <li>
                                    <img src="{!! asset('storage/avatar/'.$userdata->avatar) !!}"
                                         class="img-responsive pic-bordered" alt="">
                                    <a href="javascript:;" onclick="edit_picture()" class="profile-edit"> edit </a>
                                </li>
                                <li>
                                    <a href="javascript:;" onclick="edit_profile()"> Settings </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-12 profile-info">
                                    <h1 class="font-green sbold uppercase">{!! $userdata->name !!}
                                        ({!! $userdata->user_type !!})</h1>
                                    <p>{!! $userdata->about !!}</p>
                                    <ul class="list-inline">
                                        <li>
                                            <i class="fa fa-map-marker"></i> {!! $userdata->city !!} </li>
                                        <li>
                                            <i class="fa fa-calendar"></i> {!! $userdata->dob !!} </li>
                                        <li>
                                            <i class="fa fa-mobile"></i> {!! $userdata->mobile !!} </li>
                                        <li>
                                            <i class="fa fa-user"></i> {!! $userdata->user_type !!} </li>
                                        <li>
                                            <i class="fa fa-envelope"></i> {!! $userdata->email !!} </li>
                                    </ul>
                                </div>
                            </div>
                            <!--end row-->
                        </div>
                    </div>
                </div>
                <!--tab_1_2-->
                <div class="tab-pane" id="tab_1_3">
                    <div class="row profile-account">
                        <div class="col-md-3">
                            <ul class="ver-inline-menu tabbable margin-bottom-10">
                                <li class="active">
                                    <a id="persoalinfo" data-toggle="tab" href="#tab_1-1">
                                        <i class="fa fa-cog"></i> Personal info </a>
                                    <span class="after"> </span>
                                </li>
                                <li>
                                    <a id="changepic" data-toggle="tab" href="#tab_2-2">
                                        <i class="fa fa-picture-o"></i> Change Avatar </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab_3-3">
                                        <i class="fa fa-lock"></i> Change Password </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-9">
                            <div class="tab-content">
                                <div id="tab_1-1" class="tab-pane active">
                                    <form role="form" action="{!! route('profile-update-detail') !!}" method="post">
                                        {!! csrf_field() !!}
                                        <div class="form-group">
                                            <label class="control-label">Full Name</label>
                                            <input type="text" placeholder="Full Name" name="name"
                                                   value="{!! $userdata->name !!}"
                                                   class="form-control"></div>
                                        <div class="form-group">
                                            <label class="control-label">Email</label>
                                            <input type="text" placeholder="Email" name="email"
                                                   value="{!! $userdata->email !!}"
                                                   class="form-control"></div>
                                        <div class="form-group">
                                            <label class="control-label">Mobile Number</label>
                                            <input type="text" placeholder="+92 1234567890"
                                                   value="{!! $userdata->mobile !!}" name="mobile" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">City</label>
                                            <input type="text" placeholder="City" name="city"
                                                   value="{!! $userdata->city !!}"
                                                   class="form-control"></div>
                                        <div class="form-group">
                                            <label class="control-label">Date of Birth</label>
                                            <div class="input-group input-medium date date-picker"
                                                 data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                                                <input type="text" class="form-control" name="dob" readonly=""
                                                       value="{!! $userdata->dob !!}">
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">About</label>
                                            <textarea class="form-control" name="about" rows="3"
                                                      placeholder="We are KeenThemes!!!">{!! $userdata->about !!}</textarea>
                                        </div>
                                        <div class="margiv-top-10">
                                            <button type="submit" class="btn green">Save Changes</button>
                                            <a href="javascript:;" class="btn default"> Cancel </a>
                                        </div>
                                    </form>
                                </div>
                                <div id="tab_2-2" class="tab-pane">
                                    <form action="{!! route('profile-update-avatar') !!}" method="post" role="form"
                                          class="avatar" enctype="multipart/form-data">
                                        {!! csrf_field() !!}
                                        <div class="form-group">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail"
                                                     style="width: 200px; height: 150px;">
                                                    <img id="img_view" style="width:200px; height:140px;" src="/{!! ($userdata->avatar == '') ? 'storage/avatar/default.png' : $userdata->avatar !!}" alt="">
                                                </div>
                                                <div>
                                                    <span class="btn default btn-file">
                                                        <span class="fileinput-new"> Browes image </span>
                                                        <span class="fileinput-exists"> Change </span>
                                                        <input type="file" name="avatar" id="user-image-file-reset">
                                                    </span>
                                                    <a href="javascript:;" class="btn default fileinput-exists"
                                                       data-dismiss="fileinput" id="btn-user-image-file-reset">
                                                        Remove
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="margin-top-10">
                                            <button type="submit" class="btn green">Upload</button>
                                            <a href="javascript:;" class="btn default"> Cancel </a>
                                        </div>
                                    </form>
                                </div>
                                <div id="tab_3-3" class="tab-pane">
                                    @if (session('error'))
                                        <div class="alert alert-danger">
                                            {{ session('error') }}
                                        </div>
                                    @endif
                                    @if (session('success'))
                                        <div class="alert alert-success">
                                            {{ session('success') }}
                                        </div>
                                    @endif

                                    <form id="form_change_pass" role="form" action="{!! route('profile-update-password') !!}" method="POST">

                                        {!! csrf_field() !!}

                                        <div class="form-group {{ $errors->has('current-password') ? ' has-error' : '' }}">
                                            <label class="control-label" for="new-password">Current Password</label>
                                            <input type="password" id="current_password" name="current-password"
                                                   class="form-control" required>
                                            @if ($errors->has('current-password'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('current-password') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group {{ $errors->has('new-password') ? ' has-error' : '' }}">
                                            <label class="control-label" for="new-password">New Password</label>
                                            <input type="password" id="new_password" name="new-password" required class="form-control">
                                            @if ($errors->has('new-password'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('new-password') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="new-password-confirm" class="control-label">Re-type New Password</label>
                                            <input id="new_password_confirm" type="password" name="new-password_confirmation" required class="form-control">
                                        </div>

                                        <div class="margin-top-10">
                                            <button type="submit" class="btn green">Change Password</button>
                                            {{--<a href="javascript:;" class="btn green"> Change Password </a>--}}
                                            <a href="javascript:;" class="btn default"> Cancel </a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!--end col-md-9-->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('PAGE_LEVEL_PLUGIN')
    <script src="{{asset('public/assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('public/assets/pages/scripts/profile-2.min.js') }}" type="text/javascript"></script>
@endsection


@section('PAGE_LEVEL_SCRIPT')
    <script src="{!! asset('public/assets/pages/scripts/components-date-time-pickers.min.js') !!}" type="text/javascript"></script>
@endsection


@section('js')
    <script>
        function edit_profile() {
            $("#accounttab").click();
            $("#persoalinfo").click();
        }
        function edit_picture() {
            $("#accounttab").click();
            $("#changepic").click();
        }

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#img_view').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#user-image-file-reset").change(function() {
            readURL(this);
        });
    </script>
@endsection