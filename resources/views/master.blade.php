<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
{{--<meta name="viewport" content="width=device-width, initial-scale=1">--}}

<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') SYMBIOTEE </title>

    <!-- Styles -->
{{--    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">--}}
{{--    <link href="{{ asset('public/css/loader.css') }}" rel="stylesheet">--}}

{{--Begin Theme Style--}}

<!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="{!! asset('public/assets/global/css/open-sans.css') !!}" rel="stylesheet" type="text/css"/>
    <link href="{!! asset('public/assets/global/plugins/font-awesome/css/font-awesome.min.css') !!}" rel="stylesheet"
          type="text/css">
    <link href="{!! asset('public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') !!}" rel="stylesheet"
          type="text/css"/>
    <link href="{!! asset('public/assets/global/plugins/bootstrap/css/bootstrap.min.css') !!}" rel="stylesheet"
          type="text/css"/>
    <link href="{!! asset('public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') !!}" rel="stylesheet"
          type="text/css"/>
@yield('GLOBAL_MANDATORY')
<!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL PLUGINS -->
@yield('PAGE_LEVEL')
<!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{!! asset('public/assets/global/css/components.min.css') !!}" rel="stylesheet" type="text/css"/>
    <link href="{!! asset('public/assets/global/css/plugins.min.css') !!}" rel="stylesheet" type="text/css"/>
@yield('THEME_GLOBAL')

<!-- END THEME GLOBAL STYLES -->

    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="{!! asset('public/assets/layouts/layout2/css/layout.min.css') !!}" rel="stylesheet" type="text/css"/>
    <link href="{!! asset('public/assets/layouts/layout2/css/themes/grey.min.css') !!}" rel="stylesheet" type="text/css"
          id="style_color"/>
    <link href="{!! asset('public/assets/layouts/layout2/css/custom.min.css') !!}" rel="stylesheet" type="text/css"/>
@yield('THEME_LAYOUT')

<!-- END THEME LAYOUT STYLES -->


    {{--<link href="{!! asset('public/assets/global/plugins/bootstrap-toastr/toastr.min.css') !!}" rel="stylesheet" type="text/css">--}}
    @yield('css')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    {{--End Theme Style--}}

    <link rel="shortcut icon" href="{!! asset('favicon.ico') !!}"/>


</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">

@include('sections.header')

<div class="clearfix"></div>

<div class="page-container" id="app">

    @include('sections.side_bar')

    <div class="page-content-wrapper">
        <div class="page-content">
            <div id="loader" class="hide"></div>
            @yield('content')
        </div>
    </div>

    @include('sections.footer')
</div>

<!-- BEGIN CORE PLUGINS -->
<script src="{!! asset('public/assets/global/plugins/jquery.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('public/assets/global/plugins/bootstrap/js/bootstrap.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('public/assets/global/plugins/js.cookie.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') !!}"
        type="text/javascript"></script>
<script src="{!! asset('public/assets/global/plugins/jquery.blockui.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') !!}"
        type="text/javascript"></script>
@yield('CORE')
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
@yield('PAGE_LEVEL_PLUGIN')
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="{!! asset('public/assets/global/scripts/app.min.js') !!}" type="text/javascript"></script>
@yield('THEME_GLOBAL')
<!-- END THEME GLOBAL SCRIPTS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
@yield('PAGE_LEVEL_SCRIPT')
<!-- END PAGE LEVEL SCRIPTS -->


<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="{!! asset('public/assets/layouts/layout2/scripts/layout.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('public/assets/layouts/layout2/scripts/demo.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('public/assets/layouts/global/scripts/quick-sidebar.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('public/assets/layouts/global/scripts/quick-nav.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('public/assets/global/plugins/bootstrap-toastr/toastr.min.js') !!}" type="text/javascript"></script>
{{--<script src="{!! asset('public/js/app.js') !!}" type="text/javascript"></script>--}}
@yield('THEME_LAYOUT')
<!-- END THEME LAYOUT SCRIPTS -->

@yield('js')
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css"></script>
{{--End Base Script--}}

<script>
    $(document).ready(function () {
        $('#clickmewow').click(function () {
            $('#radio1003').attr('checked', 'checked');
        });
    })
</script>

<script>

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "200",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
            @if(Session::has('message'))

    var type = "{{Session::get('alert-type')}}";

    switch (type) {
        case "info":
            toastr.info("{{Session::get('message')}}");
            break;

        case "warning":
            toastr.warning("{{Session::get('message')}}");
            break;

        case "success":
            toastr.success("{{Session::get('message')}}");
            break;

        case "error":
            toastr.error("{{Session::get('message')}}");
            break;
    }
    @endif
</script>
</body>
</html>

